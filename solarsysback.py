import numpy as np
from matplotlib import animation
import matplotlib.pyplot as plt
from matplotlib import animation
from astropy import constants

N=10
M = np.zeros(N)#of bodies

M[0] = 3.33e5# Sun
M[1] = 5.50e-2# Mercury
M[2] = 8.15e-1# Venus
M[3] = 1.00# Earth
M[4] = 1.08e-1# Mars
M[5] = 3.18e2# Jupiter
M[6] = 9.51e1# Saturn
M[7] = 1.45e1# Uranus
M[8]= 1.71e1# Neptune
M[9]= 2.45e-2#Pluto

G=1.18e-4 #gravitation constant
c =63239.7263 #speed of light
def classical_vel(xi):
    a = np.zeros([N,7])
    for i in range(N):
        for j in range(N):
            if i==j:
                continue
            else:
                delx = xi[i][0] - xi[j][0]
                dely = xi[i][1] - xi[j][1]
                delz = xi[i][2] - xi[j][2]
                delr = np.sqrt(delx*delx +dely*dely + delz*delz)
                denom = delr * delr * delr
                a[i][3] -= G*M[j]*delx/denom
                a[i][4] -= G*M[j]*dely/denom
                a[i][5] -= G*M[j]*delz/denom
                a[i][6] += 1
    for i in range(N):
        for j in range(3):
            a[i][j] = x[i][j+3]

    return a
def vel_gr(xi,agr):
    a = np.zeros([N,6])
    for i in range(N):
        for j in range(N):
            if i==j:
                continue
            else:
                pos_i = np.concatenate((xi[i][0:3],[0,0,0,0]),axis =None)
                pos_j = np.concatenate((xi[j][0:3],[0,0,0,0]),axis =None)

                vel_i =np.concatenate( (xi[i][3:6],[0,0,0,0]),axis =None)
                vel_j =np.concatenate( (xi[j][3:6],[0,0,0,0]),axis =None)
                delr_ji = np.subtract(pos_j,pos_i)

                mag_del_r_ji = np.linalg.norm(delr_ji)

                del_r_ji_hat = np.divide(delr_ji,mag_del_r_ji)
                newtonian_term =np.multiply(G*M[j]/(mag_del_r_ji*mag_del_r_ji),del_r_ji_hat )
                a[i][3:6] += newtonian_term[0:3]









                a[i][6]+=1



    for i in range(N):
        for j in range(3):
            a[i][j] = x[i][j+3]

    return a
def classical(xi):

    h = .0001
    k1 = np.multiply(h, classical_vel(xi))
    k2 = np.multiply(h, classical_vel( np.add(xi,np.multiply(.5,k1))  ))
    k3 = np.multiply(h , classical_vel( np.add(xi,np.multiply(.5,k2))  ))
    k4 = np.multiply(h, classical_vel( np.add(xi,k3)))


    xn = np.add(k1,np.multiply(2,k2))
    xn = np.add(xn,np.multiply(2,k3))
    xn = np.add(xn, k4)
    xn = np.add(x, np.divide(xn,6))
    return xn
def einstein_infeld_hoffmann(xi,agr):
    h = .001
    k1 = np.multiply(h, vel_gr(xi,agr))
    k2 = np.multiply(h, vel_gr( np.add(xi,np.multiply(.5,k1)), agr  ))
    k3 = np.multiply(h , vel_gr( np.add(xi,np.multiply(.5,k2)), agr  ))
    k4 = np.multiply(h, vel_gr( np.add(xi,k3),agr))


    xn = np.add(k1,np.multiply(2,k2))
    xn = np.add(xn,np.multiply(2,k3))
    xn = np.add(xn, k4)
    xn = np.add(x, np.divide(xn,6))
    return xn
fig = plt.figure()

ax = plt.axes(xlim=(-1, 1), ylim=(-1, 1))


lines = [plt.plot([], [],'o')[0] for _ in range(N)]

def init_classical():
    global x
    x =  np.zeros([N,7])


    # format of vector is as follows: x,y,z,vx,vy,vz,t


    x[0] = [0,0,0,0,0,0,0] #sun
    x[1] = [3.99e-1,0,0,0,1.04e1,0,0] #mercury
    x[2] = [7.21e-1,0,0,0,7.37e0,0,0] #venus
    x[3] = [1.00e0,0,0,0,6.27,0,0] #earth
    x[4] = [1.52,0,0,0,5.07,0,0] #mars
    x[5] = [5.19e0,0,0,0,2.75e0,0,0] #jupiter
    x[6] = [9.56e0,0,0,0,2.04e0,0,0] #saturn
    x[7] = [1.92e1,0,0,0,1.44e0,0,0] #neptune
    x[8] = [3.00e1,0,0,0,1.15e0,0,0] #uranus
    x[9] =  [3.94e1,0,0,0,9.98e-1,0,0] #not a planet

    #momentun
    p = [0,0,0]
    for i in range(1,N):
        pi = np.multiply(M[i],x[i][3:6])
        p = np.add(p,pi )
    x[0][3:6] = np.divide(p,-M[0])
    for line in lines:
        line.set_data([],[])
    return lines #return everything that must be update
def animate_classical(i):

    global x

    x = classical(x)

    for j,line in enumerate(lines):

        line.set_data([x[j][0]],[x[j][1]])

    return lines #return everything that must be updated

def init_gr():
    global xgr
    # format of vector is as follows: x,y,z,vx,vy,vz,t


    xgr[0] = [0,0,0,0,0,0,0] #sun
    xgr[1] = [3.99e-1,0,0,0,1.04e1,0,0] #mercury
    xgr[2] = [7.21e-1,0,0,0,7.37e0,0,0] #venus
    xgr[3] = [1.00e0,0,0,0,6.27,0,0] #earth
    xgr[4] = [1.52,0,0,0,5.07,0,0] #mars
    xgr[5] = [5.19e0,0,0,0,2.75e0,0,0] #jupiter
    xgr[6] = [9.56e0,0,0,0,2.04e0,0,0] #saturn
    xgr[7] = [1.92e1,0,0,0,1.44e0,0,0] #neptune
    xgr[8] = [3.00e1,0,0,0,1.15e0,0,0] #uranus
    xgr[9] =  [3.94e1,0,0,0,9.98e-1,0,0] #not a planet

    #momentun
    p = [0,0,0]
    for i in range(1,N):
        pi = np.multiply(M[i],x[i][3:6])
        p = np.add(p,pi )
    x[0][3:6] = np.divide(p,-M[0])
    global agr
    agr = classical_vel(xgr)

    for line in lines:
        line.set_data([],[])
    return lines #return everything that must be update
def animate_gr(i):

    global xgr,agr

    xgr = einstein_infeld_hoffmann(xgr,agr)
    agr = vel_gr(xgr,agr)
    for j,line in enumerate(lines):
        line.set_data(line.get_xdata()+[x[j][0]],line.get_ydata()+[x[j][1]])

    return lines #return everything that must be updated
anim = animation.FuncAnimation(fig, animate_classical, init_func=init_classical,
                               frames=5000, interval=20, blit=True)
plt.show()
