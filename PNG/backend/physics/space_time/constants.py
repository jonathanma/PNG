from astropy import constants
import math
#this file contains all the astronomical constants in the correct units
# mass is in terms of earth masses
# time is in term of earth years
# distances is in AU

me_to_kg = 5.972e+24 # ratio of me to kg
au_to_m = 1.495978707e+11 # ratio of au to m
yr_to_s = 365*24*60*60 # ratio of yrs to seconds


mass_sun = 332946.0 # mass of sun in earth masses
mass_earth = 1.0 # mass of earth in eath masses

G = constants.G.value * math.pow(au_to_m,-3) * me_to_kg * yr_to_s * yr_to_s #graviational constant, converted from m3 / (kg s2) to au3 / meyr2
c = constants.c.value * (yr_to_s/au_to_m)#speed of light from m/s to au/yr
