import sys
import math
from constants import *
import numpy as np
import sympy as sp
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
def schwarzschild_metric(f1,f2,mass,option='cart'):#f1,f2 are the source point of the field
	radius_s = 2 * G *mass * math.pow(c,-2) # schwarzschild radius
	if option == 'cart':
		rprime = math.sqrt(f1*f1+f2*f2)
	elif option == 'polar':
		rprime = f1
	g =  sp.Matrix.zeros(4,4) #setting up 4d tensor in terms of index i=0,1,2,3 t,r,theta,phi
	e0, e1, e2, e3 = sp.symbols('e0 e1 e2 e3')

	g[0,0] = -(1-radius_s/(e1-rprime))*c*c #gtt
	g[1,1] =  -(c*c)/g[0,0] #grr
	g[2,2] = (e1-rprime)*(e1-rprime) #gthetatheta
	g[3,3] = g[2,2]*sp.sin(e2)*sp.sin(e2) #gphiphi
	return g
def get_christoffel_symbols(metric):
	L =sp.tensor.array.MutableSparseNDimArray(np.zeros((4,4,4)) )
	inv_metric = metric**-1
	for i in range(4):
		for k in range(4):
			for l in range(4):
				for m in range(4):
					L[i,k,l] += .5 * inv_metric[i,m] * sp.diff(metric[m,k],'e'+str(l))
					L[i,k,l] += .5 * inv_metric[i,m] * sp.diff(metric[m,l],'e'+str(k))
					L[i,k,l] -= .5 * inv_metric[i,m] * sp.diff(metric[m,l],'e'+str(m))
	return L
def equations_of_motion(metric,christoffel_symbols=None):
	if christoffel_symbols== None:
		christoffel_symbols = get_christoffel_symbols(metric)
	e0, e1, e2, e3, de0, de1, de2, de3 = sp.symbols('e0 e1 e2 e3 de0 de1 de2 de3')
	functions = [0]*8 # system of 8 1st order differential equations
	functions[0] = de0
	functions[1] = de1
	functions[2] = de2
	functions[3] = de3 # these are the equiveilen of the first order dirvativers ,ie velocity
	for i in range(4):
		for j in range(4):
			for k in range(4):
				functions[i+4] -= christoffel_symbols[i,j,k]*sp.sympify('de'+str(j))*sp.sympify('de'+str(k))
				functions[i+4] += christoffel_symbols[0,j,k]*sp.sympify('de'+str(i))*sp.sympify('de'+str(j))*sp.sympify('de'+str(k))
	#return sp.lambdify( ( (e0, e1, e2, e3, de0, de1, de2, de3) ), np.array([1,de1,de2,de3,0,-G*mass_sun/(e1*e1)+e1*de3*de3,0,-2*de1*de3/e1]), 'numpy') newtonian base
	return sp.lambdify( ( (e0, e1, e2, e3, de0, de1, de2, de3) ), np.array(functions), 'numpy')
def Runge_Kutta_4(f,t0,y0,t1,h=.0001):
	y = y0
	yi = [y0]
	t = t0

	while t<t1:

		k1 = h*np.array(f( *tuple(y) ))
		k2 = h*np.array(f( *tuple(y+k1*.5) ))
		k3 = h*np.array(f( *tuple(y+k2*.5) ))
		k4 = h*np.array(f( *tuple(y+k3) ))
		y = y + (1.0/6.0)*(k1+2*k2+2*k3+k4)
		t = t+h
		yi+=[ np.array(y) ]
	return np.array(yi)
'''
def Dormand_Prince(f,  x0,  y0,  x1,  tol = 1.0e-5,  hmax=.01,  hmin =  .0001,  maxiter= 100):

	# we trust that the compiler is smart enough to pre-evaluate the
	# value of the constants.
	a21  =  (1.0/5.0)
	a31  = (3.0/40.0)
	a32  = (9.0/40.0)
	a41  = (44.0/45.0)
	a42   = (-56.0/15.0)
	a43   = (32.0/9.0)
	a51  =  (19372.0/6561.0)
	a52    =(-25360.0/2187.0)
	a53   = (64448.0/6561.0)
	a54   = (-212.0/729.0)
	a61   = (9017.0/3168.0)
	a62   = (-355.0/33.0)
	a63   = (46732.0/5247.0)
	a64  = (49.0/176.0)
	a65  = (-5103.0/18656.0)
	a71  = (35.0/384.0)
	a72  = (0.0)
	a73  = (500.0/1113.0)
	a74  = (125.0/192.0)
	a75  = (-2187.0/6784.0)
	a76  = (11.0/84.0)

	c2   = (1.0 / 5.0)
	c3   = (3.0 / 10.0)
	c4   = (4.0 / 5.0)
	c5   = (8.0 / 9.0)
	c6   = (1.0)
	c7   = (1.0)

	b1   = (35.0/384.0)
	b2   = (0.0)
	b3   = (500.0/1113.0)
	b4   = (125.0/192.0)
	b5   = (-2187.0/6784.0)
	b6   = (11.0/84.0)
	b7   = (0.0)

	b1p  = (5179.0/57600.0)
	b2p  = (0.0)
	b3p  = (7571.0/16695.0)
	b4p  = (393.0/640.0)
	b5p  = (-92097.0/339200.0)
	b6p  = (187.0/2100.0)
	b7p  = (1.0/40.0)

	xi = [x0]
	yi = [y0]
	h = hmax
	x = x0
	for i in range(maxiter):




	for i in range(maxiter):
	   # /* Compute the function values */
	   y = yi[-1]
	   t = y[0]
	   x = t
	   r = y[1]
	   theta = y[2]
	   phi = y[3]
	   d_t = y[4]
	   d_r = y[5]
	   d_theta = y[6]
	   d_phi = y[7]
	   k1 = np.zeros( (8,))
	   k2 = np.zeros( (8,))
	   k3 = np.zeros( (8,))
	   k4 = np.zeros( (8,))
	   k5 = np.zeros( (8,))
	   k6 = np.zeros( (8,))
	   k7 = np.zeros( (8,))
	   k8 = np.zeros( (8,))
	   for j in range(8):

		   k1[j] = f[j](t,r,theta,phi,d_t,d_r,d_theta,d_phi)
		   k2[j] = f[j](t+ c2*h,r+h*(a21*k1[j]),theta+h*(a21*k1[j]),phi+h*(a21*k1[j]),d_t+h*(a21*k1[j]),d_r+h*(a21*k1[j]),d_theta+h*(a21*k1[j]),d_phi+h*(a21*k1[j]))
		   k3[j] = f[j](t+ c3*h,r+h*(a31*k1[j]+a32*k2[j]),theta+h*(a31*k1[j]+a32*k2[j]),phi+h*(a31*k1[j]+a32*k2[j]),d_t+h*(a31*k1[j]+a32*k2[j]),d_r+h*(a31*k1[j]+a32*k2[j]),d_theta+h*(a31*k1[j]+a32*k2[j]),d_phi+h*(a31*k1[j]+a32*k2[j]))
		   k4[j] = f[j](t+ c4*h,r+h*(a41*k1[j]+a42*k2[j]+a43*k3[j]),theta+h*(a41*k1[j]+a42*k2[j]+a43*k3[j]),phi+h*(a41*k1[j]+a42*k2[j]+a43*k3[j]),d_t+h*(a41*k1[j]+a42*k2[j]+a43*k3[j]),d_r+h*(a41*k1[j]+a42*k2[j]+a43*k3[j]),d_theta+h*(a41*k1[j]+a42*k2[j]+a43*k3[j]),d_phi+h*(a41*k1[j]+a42*k2[j]+a43*k3[j]))
		   k5[j] = f[j](t+ c5*h,r+h*(a51*k1[j]+a52*k2[j]+a53*k3[j]+a54*k4[j]),theta+h*(a51*k1[j]+a52*k2[j]+a53*k3[j]+a54*k4[j]),phi+h*(a51*k1[j]+a52*k2[j]+a53*k3[j]+a54*k4[j]),d_t+h*(a51*k1[j]+a52*k2[j]+a53*k3[j]+a54*k4[j]),d_r+h*(a51*k1[j]+a52*k2[j]+a53*k3[j]+a54*k4[j]),d_theta+h*(a51*k1[j]+a52*k2[j]+a53*k3[j]+a54*k4[j]),d_phi+h*(a51*k1[j]+a52*k2[j]+a53*k3[j]+a54*k4[j]))
		   k6[j] = f[j](t+ h,r+h*(a61*k1[j]+a62*k2[j]+a63*k3[j]+a64*k4[j]+a65*k5[j]),theta+h*(a61*k1[j]+a62*k2[j]+a63*k3[j]+a64*k4[j]+a65*k5[j]),phi+h*(a61*k1[j]+a62*k2[j]+a63*k3[j]+a64*k4[j]+a65*k5[j]),d_t+h*(a61*k1[j]+a62*k2[j]+a63*k3[j]+a64*k4[j]+a65*k5[j]),d_r+h*(a61*k1[j]+a62*k2[j]+a63*k3[j]+a64*k4[j]+a65*k5[j]),d_theta+h*(a61*k1[j]+a62*k2[j]+a63*k3[j]+a64*k4[j]+a65*k5[j]),d_phi+h*(a61*k1[j]+a62*k2[j]+a63*k3[j]+a64*k4[j]+a65*k5[j]))
		   k7[j] = f[j](t+ h,r+h*(a71*k1[j]+a72*k2[j]+a73*k3[j]+a74*k4[j]+a75*k5[j]+a76*k6[j]),theta+h*(a71*k1[j]+a72*k2[j]+a73*k3[j]+a74*k4[j]+a75*k5[j]+a76*k6[j]),phi+h*(a71*k1[j]+a72*k2[j]+a73*k3[j]+a74*k4[j]+a75*k5[j]+a76*k6[j]),d_t+h*(a71*k1[j]+a72*k2[j]+a73*k3[j]+a74*k4[j]+a75*k5[j]+a76*k6[j]),d_r+h*(a71*k1[j]+a72*k2[j]+a73*k3[j]+a74*k4[j]+a75*k5[j]+a76*k6[j]),d_theta+h*(a71*k1[j]+a72*k2[j]+a73*k3[j]+a74*k4[j]+a75*k5[j]+a76*k6[j]),d_phi+h*(a71*k1[j]+a72*k2[j]+a73*k3[j]+a74*k4[j]+a75*k5[j]+a76*k6[j]))

		  # fr = -(GMr2(1−2GM/r) )

	   error = abs((b1-b1p)*k1[2]+(b3-b3p)*k3[2]+(b4-b4p)*k4[j]+(b5-b5p)*k5[j]+(b6-b6p)*k6[j]+(b7-b7p)*k7[j])
	   print('error:',error)
	   # error control
	   delta = 0.84 * pow(tol / error, (1.0/5.0))
	   if (error < tol) :
		   xi += [t + h]
		   yi += [y + h * (b1*k1+b3*k3+b4*k4+b5*k5+b6*k6)]


	   if (delta <= 0.1) :
		   h = h * 0.1
	   elif (delta >= 4.0 ) :
		   h = h * 4.0
	   else :
		   h = delta * h


	   if (h > hmax) :
		   h = hmax


	   if (x >= x1) :
		   flag = 0
		   break
	   elif (x + h > x1) :
		   h    = x1 - x
	   elif (h < hmin) :
		   flag = 1
		   break



	maxiter = maxiter - i
	if (i <= 0) :
		flag = 2

	return np.array(yi)

'''
def gen_geodsics(metric, max_r,h,del_phi):
	eqs = equations_of_motion(metric)
	num_of_paths = int(2*math.pi//del_phi)
	init_cond = np.zeros( (  num_of_paths,8  ) )
	for i in range(num_of_paths):
		init_cond[i,3] = i*del_phi
		init_cond[i,2] = np.pi/2 #note, metric is undefined at np.pi
		init_cond[i,1] = max_r
		init_cond[i,5] = 0
		init_cond[i,7] = 2*np.pi
		init_cond[i,0] = 0
		num_of_paths=1
	paths = []
	for i in range(num_of_paths):
		#paths+=[Dormand_Prince(eqs, 1, init_cond[i]  ,  10 )]#f,  x0,  y0,  x1,
		paths=Runge_Kutta_4(eqs,0,init_cond[i] ,10)
	return np.array( paths )







def main():
	fig = plt.figure()
	ax = fig.add_subplot(111)
	m = schwarzschild_metric(0,0,mass_sun)
	PATH = gen_geodsics(m, 1,.01,2*np.pi)

	r = PATH[:,1]
	p = PATH[:,3]
	print('r',r)
	print('p',p)

	#R, P = np.meshgrid(r, p)
	#Z = ((R**2 - 1)**2)
	#X, Y = R*np.cos(P), R*np.sin(P)
	X, Y = r*np.cos(p), r*np.sin(p)
	# Plot the surface.
	ax.plot(X, Y)

# Tweak the limits and add latex math labels.

	ax.set_xlabel('x')
	ax.set_ylabel('y')

	plt.show()
	#print(gen_geodsics(m, 1,.01,.30)[-1])
	return None
if __name__=="__main__":
    main()
