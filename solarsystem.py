import numpy as np
from matplotlib import animation
import matplotlib.pyplot as plt
from matplotlib import animation
from astropy import constants
G=1.18e-4 #gravitation constant in AU Y M_earth units
c = ((3e8)*(60*60*24*365.0))/(1.49e11)

def classical_vel(currentState,N,M):
    a = np.zeros([N,6])
    for i in range(N):
        for j in range(N):
            if i==j:
                continue
            else:
                pos_i = currentState[i][0:3]
                pos_j = currentState[j][0:3]

                delr_ji = pos_j-pos_i

                mag_del_r_ji = np.linalg.norm(delr_ji)

                del_r_ji_hat = delr_ji/mag_del_r_ji
                newtonian_term = (G*M[j]/(mag_del_r_ji*mag_del_r_ji))*del_r_ji_hat
                a[i][3:6] += newtonian_term


    for i in range(N):
        for j in range(3):
            a[i][j] = currentState[i][j+3]

    return a
def classical(currentState,stepSize,N,M):

    k1 = np.multiply(stepSize, classical_vel(currentState,N,M))
    k2 = np.multiply(stepSize, classical_vel( np.add(currentState,np.multiply(.5,k1)),N,M  ))
    k3 = np.multiply(stepSize , classical_vel( np.add(currentState,np.multiply(.5,k2)),N,M  ))
    k4 = np.multiply(stepSize, classical_vel( np.add(currentState,k3),N,M))


    nextState = np.add(k1,np.multiply(2,k2))
    nextState = np.add(nextState,np.multiply(2,k3))
    nextState = np.add(nextState, k4)
    nextState = np.add(currentState, np.divide(nextState,6))
    return nextState
def grVel(currentState,accel,N,M):
    a = np.zeros([N,6])
    next_Accel = np.zeros([N,3])
    for i in range(N):
        for j in range(N):
            if i==j:
                continue
            pos_i = currentState[i][0:3]
            pos_j = currentState[j][0:3]
            vel_i = currentState[i][3:6]
            vel_j = currentState[j][3:6]

            delr_ji = pos_j-pos_i
            mag_del_r_ji = np.linalg.norm(delr_ji)
            del_r_ji_hat = delr_ji/mag_del_r_ji

            delr_ij = pos_i-pos_j
            mag_del_r_ij = np.linalg.norm(delr_ij)
            del_r_ij_hat = delr_ij/mag_del_r_ij


            newtonianTerm = (G*M[j]/(mag_del_r_ji*mag_del_r_ji))*del_r_ji_hat

            fstGrTerm = vel_i@vel_j.transpose()+2*vel_j@vel_j.transpose()-4*(vel_i@vel_j.transpose())-1.5*(del_r_ij_hat@vel_j.transpose())*(del_r_ij_hat@vel_j.transpose())
            
            for k in range(N):
                if k==i:
                    continue
                pos_k = currentState[k][0:3]
                vel_k = currentState[k][3:6]

                delr_ik = pos_i-pos_k
                mag_del_r_ik = np.linalg.norm(delr_ik)
                del_r_ik_hat = delr_ik/mag_del_r_ik

                fstGrTerm -= 4*G*M[k]/mag_del_r_ik
            for k in range(N):
                if j==k:
                    continue
                pos_k = currentState[k][0:3]
                vel_k = currentState[k][3:6]

                delr_jk = pos_j-pos_k
                mag_del_r_jk = np.linalg.norm(delr_jk)
                del_r_jk_hat = delr_jk/mag_del_r_jk

                fstGrTerm-= G*M[k]/mag_del_r_jk

            fstGrTerm+=.5*(delr_ij@accel[j].transpose())

            a[i][3:6]+= newtonianTerm*(1+fstGrTerm/(c*c))
            scdGrTerm = (G*M[j]/(c*c))*(del_r_ij_hat@(4*vel_i-3*vel_j).transpose())*(vel_i-vel_j)/(mag_del_r_ij*mag_del_r_ij)
            a[i][3:6]+= scdGrTerm
            thrdGrTerm = (3.5*G*M[j]/(c*c))*accel[j]/mag_del_r_ij
            a[i][3:6] += thrdGrTerm


    for i in range(N):
        for j in range(3):
            a[i][j] = currentState[i][j+3]

        next_Accel[i] = a[i][3:6]


    return (a,next_Accel)

def generalRelativity(currentState,accel,stepSize,N,M):
    V = grVel(currentState,accel,N,M)
    nextAccel = V[1]
    k1 = np.multiply(stepSize, V[0])
    k2 = np.multiply(stepSize, grVel( np.add(currentState,np.multiply(.5,k1)) ,accel,N,M )[0])
    k3 = np.multiply(stepSize , grVel( np.add(currentState,np.multiply(.5,k2)),accel,N,M  )[0])
    k4 = np.multiply(stepSize, grVel( np.add(currentState,k3),accel,N,M)[0])


    nextState = np.add(k1,np.multiply(2,k2))
    nextState = np.add(nextState,np.multiply(2,k3))
    nextState = np.add(nextState, k4)
    nextState = np.add(currentState, np.divide(nextState,6))
    return (nextState,nextAccel)
