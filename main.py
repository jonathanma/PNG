import numpy as np
from solarsystem import *
from matplotlib import animation
import matplotlib.pyplot as plt
from matplotlib import animation
from astropy import constants
import re
import time
import sys
N=9
M = np.zeros(N)#of bodies

M[0] = 3.33e5# Sun
M[1] = 5.50e-2# Mercury
M[2] = 8.15e-1# Venus
M[3] = 1.00# Earth
M[4] = 1.08e-1# Mars
M[5] = 3.18e2# Jupiter
M[6] = 9.51e1# Saturn
M[7] = 1.45e1# Uranus
#M[8]= 1.71e1# Neptune
M[8]= 2.45e-2#Pluto
M[8]= 2.45e-2#Pluto
planetnames = ['sun',"mercury",'venus','earth','mars','jupiter','saturn','uranus','pluto']
#1900

def getExperimental(planets,numofyears):
    numofdays = 365*numofyears
    states =  np.zeros ( [numofdays,N,6])
    files = []
    startDatas = []
    dataSep = []
    for planet in planets:
        files += [open(planet+'.txt','r')]
        startDatas += [False]
        dataSep +=[0]
    neptunestart =18255 # neptunes data starts later
    j=0
    while j<numofdays:
        VorX =False
        for i in range(len(files) ):


            line = files[i].readline()

            if not startDatas[i]:

                if ''.join(line.split()) == "$$SOE":
                    startDatas[i]=True
                    j=0
                continue
            else:
                if ''.join(line.split()) == "$$EOE":
                    break

                if len(line.split())==0:
                    continue
                #print(line)
                data = re.findall("-*\d*\.\d*E\+\d*|-*\d*\.\d*E-\d*",line)
                if dataSep[i] == 0:

                    t=float(line.split()[0])
                    dataSep[i]+=1
                    continue
                if len(line.split())>=1:

                    if dataSep[i]==1:

                        states[j][i][0] = float(data[0])  #x
                        states[j][i][1] =float(data[1])  #y
                        states[j][i][2] ==float(data[2]) #z
                    elif dataSep[i]==2:
                        states[j][i][3] = float(data[0])*365#vx
                        states[j][i][4] =float(data[1])*365  #vy
                        states[j][i][5] =float(data[2]) *365 #vz
                        VorX =True
                        #print(' '.join(str(e) for e in states[j][i]),i,j)


                    dataSep[i]+=1
                    if dataSep[i]==4:
                        dataSep[i]=0
            #


        if VorX:
            delSunEarth =  states[j][0] -states[j][3]
            for i in range(len(planets)):
                states[j][i] -= delSunEarth
            j+=1
    for file in files:
        file.close()
    return states


def init_experiment_plotter():

    for j,line in enumerate(lines):
        line.set_data([],[])
    return lines
def experiment_plotter(state):

    for j,line in enumerate(lines):
        line.set_data(line.get_xdata()+[state[j][0]],line.get_ydata()+[state[j][1]])
    return lines
def animate_experiment():
    fig = plt.figure()
    ax = plt.axes(xlim=(-1, 1), ylim=(-1, 1))

    global lines
    lines = [plt.plot([], [],'*', label = '{}'.format(planetnames[i]))[0] for i in range(2)]
    states = getExperimental(planetnames,100)
    anim = animation.FuncAnimation(fig, experiment_plotter,frames=states, init_func=init_experiment_plotter,blit=True,interval=10)
    plt.legend(loc="upper left")
    plt.show()
def init_classical_plotter():
    for j,line in enumerate(lines):
        line.set_data([],[])
    return lines
def classical_plotter(i,currentState,stepSize,N,M):

    global state
    state = classical(state,stepSize,N,M)
    for j,line in enumerate(lines):
        print(j,state[j][0],state[j][1],i)
        line.set_data(line.get_xdata()+[state[j][0]],line.get_ydata()+[state[j][1]])
    return lines
def animate_classical(stepSize):
    fig = plt.figure()
    ax = plt.axes(xlim=(-1, 1), ylim=(-1, 1))
    #lines = [plt.plot([], [],'o')[0] for _ in range(N)]

    global state

    state = getExperimental(planetnames,1)[0]
    global lines
    lines = [plt.plot([], [],'o', label = '{}'.format(planetnames[i]))[0] for i in range(4)]
    anim = animation.FuncAnimation(fig, classical_plotter, init_func=init_classical_plotter,blit=True,interval=10,fargs=(state,stepSize,N,M))
    plt.legend(loc="upper left")
    plt.show()
def init_gr_plotter():
    for j,line in enumerate(lines):
        line.set_data([],[])
    return lines
def gr_plotter(i,stepSize,N,M):

    global state
    global accel
    G = generalRelativity(state,accel,stepSize,N,M)
    state = G[0]
    accel = G[1]

    for j,line in enumerate(lines):
        print(j,state[j][0],state[j][1],i)
        line.set_data(line.get_xdata()+[state[j][0]],line.get_ydata()+[state[j][1]])
    return lines
def animate_gr():
    fig = plt.figure()
    ax = plt.axes(xlim=(-5, 5), ylim=(-5, 5))
    global state
    global accel

    stepSize=.001
    state = getExperimental(planetnames,1)[0]

    accelPlace = classical_vel(state,N,M)
    accel = np.zeros( [N,3])
    for i in range(N):
        accel[i] = accelPlace[i][3:6]

    global lines
    lines = [plt.plot([], [],'o', label = '{}'.format(planetnames[i]))[0] for i in range(N)]
    anim = animation.FuncAnimation(fig, gr_plotter, init_func=init_gr_plotter,blit=True,interval=10,fargs=(stepSize,N,M))
    plt.legend(loc="upper left")
    plt.show()
#animate_experiment()
#animate_classical(.001)
#animate_gr()
def measure_diff(totaltime,timestep):# measures the percentage difference of mercury's radius away from sun

    t = np.zeros([int(totaltime/timestep)]) #time in years
    observedR = np.zeros([int(totaltime/timestep)])
    observedPhi = np.zeros([int(totaltime/timestep)])
    classicalR =np.zeros([int(totaltime/timestep)])
    classicalPhi  =np.zeros([int(totaltime/timestep)])
    grR =np.zeros([int(totaltime/timestep)])
    grPhi =np.zeros([int(totaltime/timestep)])

    observeredstates = getExperimental(planetnames,totaltime)
    initialstate = observeredstates[0]
    classicalCurrentState = initialstate
    grCurrentState = initialstate

    accelPlace = classical_vel(initialstate,N,M)
    accelGr = np.zeros( [N,3])
    for i in range(N):
        accelGr[i] = accelPlace[i][3:6]

    for i in range(int(totaltime/timestep)):
        t[i]= i*timestep

        mercuryObservedPos = observeredstates[i][3][0:3]

        observedR[i] = np.sqrt(mercuryObservedPos @ mercuryObservedPos.transpose() )

        mercuryclassPos = classicalCurrentState[1][0:3]
        classicalR[i]= np.sqrt(mercuryclassPos @ mercuryclassPos.transpose())

        mercuryGrPos = grCurrentState[1][0:3]
        grR[i] = np.sqrt( mercuryGrPos @ mercuryGrPos.transpose() )
        print(i,observedR[i] ,classicalR[i],grR[i])
        classicalCurrentState = classical(classicalCurrentState,timestep,N,M)
        G=generalRelativity(grCurrentState,accelGr,timestep,N,M)
        accelGr=G[1]
        grCurrentState = G[0]

    percentDiffClass = np.absolute( 100*(observedR-classicalR)/observedR )
    percentDiffGr = np.absolute( 100*(observedR-grR)/observedR )-.02
    fig = plt.figure()
    ax = fig.add_subplot(111)
    #ax.plot(t,percentDiffGr,label='Relativistic')
    ax.plot(t,percentDiffClass)
    ax.set_xlabel('Time (years)')
    ax.set_ylabel('% Difference')
    ax.legend()

    plt.show()
    return (percentDiffClass,percentDiffGr,t)
measure_diff(1,1/365.0)
def calc_classicalPress(totaltime,timestep):
    start = time.time()
    t = np.zeros([int(totaltime/timestep)])
    r = np.zeros([int(totaltime/timestep)])
    phi = np.zeros([int(totaltime/timestep)])
    prec = 0
    #observeredstates = getExperimental(planetnames,1)
    x =  np.zeros([N,6])
    x[0] = [0,0,0,0,0,0] #sun
    x[1] = [3.99e-1,0,0,0,1.04e1,0] #mercury
    x[2] = [7.21e-1,0,0,0,7.37e0,0] #venus
    x[3] = [1.00e0,0,0,0,6.27,0] #earth
    x[4] = [1.52,0,0,0,5.07,0] #mars
    x[5] = [5.19e0,0,0,0,2.75e0,0] #jupiter
    x[6] = [9.56e0,0,0,0,2.04e0,0] #saturn
    x[7] = [1.92e1,0,0,0,1.44e0,0] #neptune

    x[8] =  [3.94e1,0,0,0,9.98e-1,0] #not a planet
    #momentun
    p = [0,0,0]
    for i in range(1,N):
        pi = np.multiply(M[i],x[i][3:6])
        p = np.add(p,pi )
    x[0][3:6] = np.divide(p,-M[0])
    initialstate = x
    classicalCurrentState = initialstate
    mini = -1
    delphi=0

    for i in range(int(totaltime/timestep)-2):
        t[i]= i*timestep
        mercuryclassPos = classicalCurrentState[5][0:3]
        if i<=1:
            r[i] = np.sqrt(mercuryclassPos @ mercuryclassPos.transpose())
            phi[i] = np.arctan2(mercuryclassPos[1],mercuryclassPos[0])+np.pi

            classicalCurrentState = classical(classicalCurrentState,timestep,N,M)
            continue
        r[i] = np.sqrt(mercuryclassPos @ mercuryclassPos.transpose())
        phi[i] = np.arctan2(mercuryclassPos[1],mercuryclassPos[0])+np.pi
        print(i,r[i],phi[i])
        if phi[i]>=0 and phi[i-1]>=0:
            delphi+=phi[i]-phi[i-1]
        elif phi[i]>=0 and phi[i-1]<0:
            delphi+=phi[i] +phi[i-1]

        if r[i-2]>r[i-1] and r[i]>r[i-1]:
            if mini!=-1:

                print(delphi,prec,phi[i-1],phi[mini],i)
                prec +=  delphi
                delphi=0
                mini = i-1
            else:
                delphi = 0
                mini = i-1


        classicalCurrentState = classical(classicalCurrentState,timestep,N,M)
    return prec,time.time()-start
def calc_grPress(totaltime,timestep):
    start = time.time()
    t = np.zeros([int(totaltime/timestep)])
    r = np.zeros([int(totaltime/timestep)])
    phi = np.zeros([int(totaltime/timestep)])
    prec = 0
    #observeredstates = getExperimental(planetnames,1)
    x =  np.zeros([N,6])
    x[0] = [0,0,0,0,0,0] #sun
    x[1] = [3.99e-1,0,0,0,1.04e1,0] #mercury
    x[2] = [7.21e-1,0,0,0,7.37e0,0] #venus
    x[3] = [1.00e0,0,0,0,6.27,0] #earth
    x[4] = [1.52,0,0,0,5.07,0] #mars
    x[5] = [5.19e0,0,0,0,2.75e0,0] #jupiter
    x[6] = [9.56e0,0,0,0,2.04e0,0] #saturn
    x[7] = [1.92e1,0,0,0,1.44e0,0] #neptune

    x[8] =  [3.94e1,0,0,0,9.98e-1,0] #not a planet
    #momentun
    p = [0,0,0]
    for i in range(1,N):
        pi = np.multiply(M[i],x[i][3:6])
        p = np.add(p,pi )
    x[0][3:6] = np.divide(p,-M[0])
    initialstate = x
    grCurrentState = initialstate
    mini = -1
    delphi=0
    accelPlace = classical_vel(initialstate,N,M)
    accelGr = np.zeros( [N,3])
    for i in range(N):
        accelGr[i] = accelPlace[i][3:6]


    for i in range(int(totaltime/timestep)-2):
        t[i]= i*timestep
        mercuryclassPos = grCurrentState[1][0:3]
        if i<=1:
            r[i] = np.sqrt(mercuryclassPos @ mercuryclassPos.transpose())
            phi[i] = np.arctan2(mercuryclassPos[1],mercuryclassPos[0])+np.pi

            G=generalRelativity(grCurrentState,accelGr,timestep,N,M)
            accelGr=G[1]
            grCurrentState = G[0]
            continue
        r[i] = np.sqrt(mercuryclassPos @ mercuryclassPos.transpose())
        phi[i] = np.arctan2(mercuryclassPos[1],mercuryclassPos[0])+np.pi
        print(i,r[i],phi[i])
        if phi[i]>=0 and phi[i-1]>=0:
            delphi+=phi[i]-phi[i-1]
        elif phi[i]>=0 and phi[i-1]<0:
            delphi+=phi[i] +phi[i-1]

        if r[i-2]>r[i-1] and r[i]>r[i-1]:
            if mini!=-1:

                print(delphi,prec,phi[i-1],phi[mini],i)
                prec +=  delphi
                delphi=0
                mini = i-1
            else:
                delphi = 0
                mini = i-1


        G=generalRelativity(grCurrentState,accelGr,timestep,N,M)
        accelGr=G[1]
        grCurrentState = G[0]
    return prec,time.time()-start
def calc_observedPress(totaltime,timestep):

    t = np.zeros([int(totaltime/timestep)])
    r = np.zeros([int(totaltime/timestep)])
    phi = np.zeros([int(totaltime/timestep)])
    prec = 0
    observeredstates = getExperimental(planetnames,totaltime)
    mini = -1
    delphi=0
    for i in range(int(totaltime/timestep)-2):
        t[i]= i*timestep
        mercuryObservedPos = observeredstates[i][1][0:3]
        if i<=1:
            r[i] = np.sqrt(mercuryObservedPos @ mercuryObservedPos.transpose())
            phi[i] = np.arctan2(mercuryObservedPos[1],mercuryObservedPos[0])+np.pi


            continue
        r[i] = np.sqrt(mercuryObservedPos @ mercuryObservedPos.transpose())
        phi[i] = np.arctan2(mercuryObservedPos[1],mercuryObservedPos[0])+np.pi
        if phi[i]>=0 and phi[i-1]>=0:
            delphi+=phi[i]-phi[i-1]
        elif phi[i]>=0 and phi[i-1]<0:
            delphi+=phi[i] +phi[i-1]

        if r[i-2]>r[i-1] and r[i]>r[i-1]:
            if mini!=-1:

                print(delphi,prec,phi[i-1],phi[mini],i)
                prec += delphi
                delphi=0
                mini = i-1
            else:
                delphi = 0
                mini = i-1

    return prec

#print('results',calc_classicalPress(100,10e-2))

#animate_classical(10e-3)
#print(calc_classicalPress(100,10e-5))      1`
option = int(sys.argv[1])
timestepin= float(sys.argv[2])
years = int(sys.argv[3])
if option==0:
    print(calc_classicalPress(years,timestepin))
if option==1:
    print(calc_grPress(years,timestepin))
if option == 2:
    print(calc_observedPress(1,1/365.0))
