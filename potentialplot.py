import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
n=40000
h=.0001
r = np.zeros([n])
Vc = np.zeros([n])
Vr = np.zeros([n])
f =4.3
for i in range(n):

    r[i]=h*i+.01
    r2= r[i]*r[i]
    Vc[i]= -1/r[i]+(f*f)/(2*r2)
    Vr[i]= Vc[i]-(f*f)/(r2*r[i])

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(r,Vr,'-',label='Relativistic')
ax.plot(r,Vc,label='Classical')

scale_x = 1
scale_y = 1
ticks_x = ticker.FuncFormatter(lambda r, pos: '{0:g}'.format(r/scale_x))
ax.xaxis.set_major_formatter(ticks_x)

ticks_y = ticker.FuncFormatter(lambda r, pos: '{0:g}'.format(r/scale_y))
ax.yaxis.set_major_formatter(ticks_y)

ax.set_xlabel('r/M')
ax.set_ylabel('Potentials')
ax.legend()

plt.show()
