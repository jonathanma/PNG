
import re
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from astropy import constants

#constants
G = 1.18e-4

planetObjMass = {
'sun':3.33e5,
'venus':8.15e-1,
'mars': 1.08e-1,
'earth':1,
'mercury':5.50e-2,
'jupiter':3.18e2

}
def importData(filename):
    file = open(filename,'r')
    startData = False
    x=[]
    y=[]
    z=[]
    vx=[]
    vy=[]
    vz=[]
    t=[]
    i=0
    for line in file.readlines():

        if not startData:

            if ''.join(line.split()) == "$$SOE":
                startData=True
            continue
        else:
            if ''.join(line.split()) == "$$EOE":
                break

            if len(line.split())==0:
                continue
            data = re.findall("-*\d*\.\d*E\+\d*|-*\d*\.\d*E-\d*",line)
            if i == 0:
                t.append(float(line.split()[0]))
                i+=1
                continue
            if len(line.split())>=1:
                if i==1:

                    x.append(float(data[0]) )
                    y.append(float(data[1]) )
                    z.append(float(data[2]) )
                elif i==2:
                    vx.append(float(data[0]) )
                    vy.append(float(data[1]) )
                    vz.append(float(data[2]) )

                i+=1
                if i==4:
                    i=0

    return (x,y,z,vx,vy,vz,t)


planetaryObjNames= ['sun',"mercury",'venus','earth','mars','jupiter']#,"mercury","venus","earth","mars","earth","uranus","neptune","pluto"]
planetaryObj={}

for planet in planetaryObjNames:
    planetaryObj[planet]=importData(planet+".txt")
cm = [[],[],[],[],[],[]] #cm=center of mass
M = 0
for planet in planetaryObjNames:
    M+= planetObjMass[planet]
for i in range(len(planetaryObj['sun'][0])):

    for j in range(len(cm)):
        cm[j].append(0)
    for planet in planetaryObjNames:
        for j in range(len(cm)):
            cm[j][i]+=planetObjMass[planet]*planetaryObj[planet][j][i]/M
for planet in planetaryObjNames:
    for i in range(len(cm[0])):
        for j in range(len(cm)):
            planetaryObj[planet][j][i]= planetaryObj[planet][j][i]-cm[j][i]
fig, ax = plt.subplots()


minX= -10
maxX= 10
minY= -10
maxY= 10
minZ= -10
maxZ= 10

lns,=plt.plot([],[],'ro')
xdata=[]
ydata=[]


def init():
    ax.set_xlim(minX,maxX )
    ax.set_ylim(minY,maxY )
    return lns,

def experiment(i):

    xdata = []
    ydata = []
    for planet in planetaryObjNames:

        xdata.append(planetaryObj[planet][0][i])
        ydata.append(planetaryObj[planet][1][i])
        x=planetaryObj[planet][0][i]
        y=planetaryObj[planet][1][i]
        z=planetaryObj[planet][2][i]
        vx=planetaryObj[planet][3][i]
        vy=planetaryObj[planet][4][i]
        vz=planetaryObj[planet][5][i]

        r = np.sqrt( x**2 + y**2 + z**2)
        v_r= (x*vx+y*vy+z*vz)/r
        phi = np.arctan(y/x)
        v_phi = (np.square(np.cos(phi)) )* (x*vy-y*vx)/(np.square(x))
        theta = np.arccos(z/r)
        v_theta = -(1/np.sin(theta)) * (vz*r - v_r*z)/(np.square(r))
        #print(i,planet,": r:",r,"v_r:",v_r)#,"theta :",theta,"v_theta :",v_theta,"phi :",theta,"v_phi :",v_theta)
        print(planet,x,y,z,vx,vy,vz)
        #print(planet,i,r,theta,phi,v_r,v_theta,v_phi)
    lns.set_data(xdata, ydata)


    return lns,
#v=au/y *(y/d)

initialState = {}
p = [0,0,0]
initialState['sun']=[0,0,0,0,0,0]
initialState['mercury']=[3.99e-1,0,0,0,10.4,0]
initialState['venus']=[7.21e-1,0,0,0,7.37,0]
initialState['earth']=[1,0,0,0,6.27,0]
initialState['mars']=[1.52,0,0,0,5.07,0]
initialState['jupiter']=[5.19,0,0,0,2.75,0]


for planet in planetaryObjNames:
    #initialState[planet] =[]
    for i in range(6):
        xzzz=0
        #initialState[planet].append(planetaryObj[planet][i][0])
    p[0] += planetObjMass[planet]*initialState[planet][3]
    p[1] += planetObjMass[planet]*initialState[planet][4]
    p[2] += planetObjMass[planet]*initialState[planet][5]
initialState['sun'][3]=-p[0]/planetObjMass['sun']
initialState['sun'][4]=-p[1]/planetObjMass['sun']
initialState['sun'][5]=-p[2]/planetObjMass['sun']

currentState=initialState
def classical_V(x,y,z,vx,vy,vz,planet):
    global currentState
    if planet == 'sun':
        Fx=0
        Fy=0
        Fz=0
    else:
        R=np.sqrt(x*x+y*y)
        R2= R*np.sqrt(x*x+y*y)
        Fx=G*planetObjMass['sun']*x/R2
        Fy=G*planetObjMass['sun']*y/R2
        Fz=0

    '''
    for potentialSrc in planetaryObjNames:
        if potentialSrc== planet:
            continue
        xsrc = currentState[potentialSrc][0]
        ysrc = currentState[potentialSrc][1]
        zsrc = currentState[potentialSrc][0]
        delr = [x-xsrc,y-ysrc,z-zsrc]
        delrmag= np.sqrt(np.square(delr[0])+np.square(delr[1])+np.square(delr[2]))

        Fx +=G*planetObjMass[potentialSrc]*delr[0]/(delrmag**3)
        Fy +=G*planetObjMass[potentialSrc]*delr[1]/(delrmag**3)
        Fz +=G*planetObjMass[potentialSrc]*delr[2]/(delrmag**3)


        #print(planet,potentialSrc,G*planetObjMass[potentialSrc]/(displacement**2),displacement)
        '''
    next_vx = -Fx
    next_vy = -Fy
    next_vz = -Fz
    return (next_vx,next_vy,next_vz)
def classical(i):

    nextState ={}
    global currentState
    xdata=[]
    ydata=[]
    for planet in planetaryObjNames:
        #if planet=='sun':
        #    nextState['sun']= currentState['sun']

        #    continue
        x=currentState[planet][0]
        y=currentState[planet][1]
        z=currentState[planet][2]
        vx=currentState[planet][3]
        vy=currentState[planet][4]
        vz=currentState[planet][5]


        #print(i,planet,": r:",r,"v_r:",v_r)#,"theta :",theta,"v_theta :",v_theta,"phi :",theta,"v_phi :",v_theta)
        print(planet,i,x,y,z,vx,vy,vz)
        h = .001 #step size, with units of days
        #r,v_r,theta, v_theta,phi, v_phi
        V1=classical_V(x,y,z,vx,vy,vz,planet)
        k1 = (h*vx, h*V1[0], h*vy,V1[1],h*vz,V1[2])
        V2=classical_V(x+k1[0]/2,y+k1[2]/2,z+k1[4]/2,vx+k1[1]/2,vy+k1[3]/2,vz+k1[5]/2,planet)
        k2 = (h*vx, h*V2[0], h*vy,V2[1],h*vz,h*V2[2])
        V3=classical_V(x+k2[0]/2,y+k2[2]/2,z+k2[4]/2,vx+k2[1]/2,vy+k2[3]/2,vz+k2[5]/2,planet)
        k3 = (h*vx, h*V3[0], h*vy,V3[1],h*vz,h*V3[2])
        V4=classical_V(x+k3[0],y+k3[2],z+k3[4],vx+k3[1],vy+k3[3],vz+k3[5],planet)
        k4 = (h*vx, h*V4[0], h*vy,V4[1],h*vz,h*V4[2])

        next_x = x+(k1[0]+2*k2[0]+2*k3[0]+k4[0])/6
        next_vx = vx+(k1[1]+2*k2[1]+2*k3[1]+k4[1])/6
        next_y = y+(k1[2]+2*k2[2]+2*k3[2]+k4[2])/6
        next_vy = vy+(k1[3]+2*k2[3]+2*k3[3]+k4[3])/6
        next_z = z+(k1[4]+2*k2[4]+2*k3[4]+k4[4])/6
        next_vz = vz+(k1[5]+2.*k2[5]+2*k3[5]+k4[5])/6
        print(planet,i,next_x,next_y,next_z,next_vx,next_vy,next_vz)

        xdata.append(next_x)
        ydata.append(next_y)
        nextState[planet]= [next_x,next_y,next_z,next_vx,next_vx,next_vy,next_vz]
    currentState=nextState
    lns.set_data(xdata, ydata)


    return lns,

ani = FuncAnimation(fig, experiment,len(planetaryObj['sun'][0]),init_func=init, blit=True)
#ani = FuncAnimation(fig,classical,len(planetaryObj['sun'][0]),init_func=init, blit=True)
plt.show()
